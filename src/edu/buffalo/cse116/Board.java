package edu.buffalo.cse116;

import java.util.ArrayList;

public class Board {


	protected static String[][] _boardDimensions; // will store the dimensions of the board in a 2d array

	protected static ArrayList<Room> listOfRooms;

	public Board() {
		//System.out.println("Can you hear me");
		
		listOfRooms = new ArrayList<Room>();
//		_boardDimensions = new String[25][24];
		_boardDimensions = new String[24][25];
		//System.out.println("Test1");
//		createStartSpaces();
		
		//System.out.println("Test2");
		createRooms();
		
//		System.out.println("Test3");
		door();
		
//		System.out.println("Test4");
		hallway();
//		System.out.println("Test5");
		createOtherSpaces();
		secretPlaces();
		//_boardDimensions[7][0] = "Hallway";


	}

	public void createRooms() {
//		System.out.println("Test2a");

		Room Kitchen = new Room(0, 6, 0, 5, _boardDimensions, "Kitchen");
//		System.out.println("TestKitchenCompletion");
		listOfRooms.add(Kitchen);
		Room DiningRoom = new Room(9, 15, 0, 4, 10, 15, 5, 7, _boardDimensions, "DiningRoom");
//		System.out.println("DiningRoomCompletion");
		listOfRooms.add(DiningRoom);
		Room Lounge = new Room(19, 24, 0, 6, _boardDimensions, "Lounge");
//		System.out.println("LoungeCompletion");
		listOfRooms.add(Lounge);
		Room Hall = new Room(18, 23, 9, 14, 24, 24, 10, 13, _boardDimensions, "Hall");
//		System.out.println("HallCompletion");
		listOfRooms.add(Hall);
		Room Study = new Room(21, 23, 17, 23, 24, 24, 18, 23, _boardDimensions, "Study");
//		System.out.println("StudyCompletion");
		listOfRooms.add(Study);
		Room Library = new Room(14, 18, 18, 22, 15, 17, 17, 23, _boardDimensions, "Library");
//		System.out.println("LibraryCompletion");
		listOfRooms.add(Library);
		Room BillardRoom = new Room(8, 12, 18, 23, _boardDimensions, "BillardRoom");
//		System.out.println("BillardRoomCompletion");
		listOfRooms.add(BillardRoom);
		Room Conservatory = new Room(1, 4, 18, 23, 1, 5, 19, 22, _boardDimensions, "Conservatory");
//		System.out.println("ConservatoryCompletion");
		listOfRooms.add(Conservatory);
		Room BallRoom = new Room(2, 7, 8, 15, 0, 7, 10, 13, _boardDimensions, "BallRoom");
//		System.out.println("BallRoomCompletion");
		listOfRooms.add(BallRoom);
		Room CenterBox = new Room(10, 16, 10, 14, _boardDimensions, "CENTER");
		listOfRooms.add(CenterBox);
		
//		System.out.println("CenterBoxCompletion");
	}
	
	public static boolean isHallway(int x, int y) {
		if (_boardDimensions[x][y] == "Hallway" || _boardDimensions[x][y] == "StartSpace1" || _boardDimensions[x][y] == "StartSpace2" || _boardDimensions[x][y] == "StartSpace3" || _boardDimensions[x][y] == "StartSpace4" || _boardDimensions[x][y] == "StartSpace5" || _boardDimensions[x][y] == "StartSpace6") {
			return true;
		} else {
			return false;
		}
	}

	public void door() {
		_boardDimensions[4][6] = "Door";// Kitchen Door
		_boardDimensions[6][15] = "Door";// Dining Room Door
		_boardDimensions[6][19] = "Door";// Lounge Door
		_boardDimensions[12][18] = "Door";// Hall Door
		_boardDimensions[17][21] = "Door";// Study Door
		_boardDimensions[17][16] = "Door";// Library Door
		_boardDimensions[18][9] = "Door";// Billiard Room Door
		_boardDimensions[19][5] = "Door"; // Conservatory Door
		_boardDimensions[15][5] = "Door";
		_boardDimensions[9][7] = "Door";// BallRoom Doors (2)
		createEntrances();
	}

	public void hallway() {
		// replacing all other spaces left with "Hallway"
		for (int i = 0; i < _boardDimensions.length; i++) {
			for (int y = 0; y < _boardDimensions[0].length; y++) {
				if (_boardDimensions[i][y] == null) {
					_boardDimensions[i][y] = "Hallway";
				}
			}
		}
		// after creating the hallways, create the null spaces
		createOtherSpaces();
	}

	public void createOtherSpaces() {
		for (int i = 6; i < 9; i++) {
			_boardDimensions[i][0] = null;
		}
		for (int i = 15; i < 24; i++) {
			_boardDimensions[i][0] = null;
		}
//		for (int i = 9; i < 15; i++){
//			_boardDimensions[i][9] = "Hallway";
//		}

		_boardDimensions[0][8] = null;
		_boardDimensions[0][6] = null;
		_boardDimensions[23][5] = null;
//		_boardDimensions[18][5] = "Hallway";
		_boardDimensions[23][7] = null;
		_boardDimensions[23][14] = null;
//		_boardDimensions[6][19] = "Hallway";
//		_boardDimensions[17][16] = "Hallway";
//		_boardDimensions[7][0] = "Hallway";
//		_boardDimensions[8][0] = null;
//		_boardDimensions[18][0] = null;
//		_boardDimensions[16][0] = null;
//		_boardDimensions[23][8] = null;
		_boardDimensions[23][14] = null;
//		_boardDimensions[20][24] = null;
		_boardDimensions[17][24] = null;
		_boardDimensions[14][24] = null;
		_boardDimensions[15][24] = null; 
		_boardDimensions[6][24] = null;
		_boardDimensions[8][24] = null;
		_boardDimensions[9][24] = null;

	}
//
//	public void createStartSpaces() {
//
//		_boardDimensions[9][0] = "StartSpace1";
//		_boardDimensions[14][0] = "StartSpace2";
//		_boardDimensions[7][24] = "StartSpace3";
//		_boardDimensions[23][19] = "StartSpace4";
//		_boardDimensions[23][6] = "StartSpace5";
//		_boardDimensions[0][17] = "StartSpace6";
//
//	}

	public void secretPlaces() {
		_boardDimensions[5][1] = "KitchenSP";
		_boardDimensions[0][19] = "LoungeSP";
		_boardDimensions[23][21] = "StudySP";
		_boardDimensions[22][5] = "ConservatorySP";
	}

	public void createEntrances(){
		_boardDimensions[4][7] = "DoorEntrance";//Kitchen Entrance 
//		_boardDimensions[12][8] = "DoorEntrance";// Dining Room Entrance
		_boardDimensions[6][16] = "DoorEntrance";// Dining Room Entrance2
		_boardDimensions[6][18] = "DoorEntrance";// Lounge Entrance
		_boardDimensions[12][17] = "DoorEntrance";// Hall Entrance
//		_boardDimensions[20][15] = "DoorEntrance";// Hall Entrance
//		_boardDimensions[17][11] = "DoorEntrance";// Hall Entrance
		_boardDimensions[17][20] = "DoorEntrance";// Study Entrance
		_boardDimensions[16][16] = "DoorEntrance";// Library Entrance
//		_boardDimensions[13][20] = "DoorEntrance";// Library Entrance
		_boardDimensions[17][9] = "DoorEntrance";// Billiard Room Entrance
//		_boardDimensions[13][23] = "DoorEntrance";// Billiard Room Entrance
		_boardDimensions[18][5] = "DoorEntrance"; // Conservatory Entrance
		_boardDimensions[16][5] = "DoorEntrance";
//		_boardDimensions[8][14] = "DoorEntrance";
		_boardDimensions[9][8] = "DoorEntrance";// BallRoom Doors (4)
	}
	
	
	

	// Some getters for when we do have a GUI. Not need any time soon
	public String[][] getBoardDimensions() {
		return _boardDimensions;
	}
	
	
	public static boolean isRoom(int i, int j) {
		String label = _boardDimensions[i][j];
		return (label == "Kitchen" || label == "DiningRoom" || label == "Lounge" || label == "Library" || label == "BillardRoom" || label == "Conservatory" || label == "CENTER" || label == "Hall" || label == "Study" || label == "BallRoom");
	}


	
}
