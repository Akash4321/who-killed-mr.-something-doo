package edu.buffalo.cse116;

import java.util.List;
import java.util.Scanner;

public class InterClassInteractions {

	private String Input_Prompt;
	private Player player;
	private String RoomName;
	private static Cards c = new Cards();

	
	public InterClassInteractions(Player p, String room){
		player = p;
		RoomName = room;
	}
	

//	public InterClassInteractions(String s, Player p, String rN) {
//		player = p;
//		if (s == "enter") {
//			enterRoom(player);
//			RoomName = rN;
//			Input_Prompt = "Please suggest a person, a weapon and a room, in that order, with proper capitalization: ";
//		}
//	}
//
//	public void enterRoom(Player player) {
//		System.out.println(Input_Prompt);
//		Scanner sc = new Scanner(System.in);
//		player.setInARoom(true);
//		player.moveIntoRoom(player, RoomName);
//		makeSuggestion(sc);
//
//
//	}
									// player player
	public void makeSuggestion(TakeInput tI) throws NopetepousException {

		String person = tI.getPerson();
		if (c.getPeople().contains(person) == false){
			throw new NopetepousException();
		}
		String weapon = tI.getWeapon();
		if (c.getWeapons().contains(weapon) == false){
			throw new NopetepousException();
		}
		String room = tI.getRoom();
		if (c.getRooms().contains(room) == false){
			throw new NopetepousException();
		}
		Cards.suggestionCheckCurrentPlayer(person, room, weapon, player);
		Cards.suggestionRot(person, room, weapon, player);


	}
	
	
	//player player
	public static void makeAccusation(String person, String room, String weapon) {

//		String person = sc.next();
//		if (c.getPeople().contains(person) == false){
//			throw new NopetepousException();
//		}
//		String weapon = sc.next();
//		if (c.getWeapons().contains(weapon) == false){
//			throw new NopetepousException();
//		}
//		String room = sc.next();
//		if (c.getRooms().contains(room) == false){
//			throw new NopetepousException();
//		}
		Cards.accusationValidation(person, room, weapon);
	}

}

