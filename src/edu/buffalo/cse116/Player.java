package edu.buffalo.cse116;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Player {
	protected String _playerName; // Value that holds the name of the player
	protected static int _movesMade = 0; // Value that tracks the moves the
											// player has
	// made vs the number they rolled
	protected int _cursorX;
	protected int _cursorY;
	private Integer _roomX;
	private Integer _roomY;
	protected boolean _myTurn;
	protected static int _randomNumber;
	private String _currentTile;
	private String _movedFromTile;
	private static List<String> _playerHand;
	protected static ArrayList<Player> listOfPlayers = new ArrayList<Player>();
	private static Cards _cards;
	boolean _inARoom;
	protected String inRoom;
	private static int playerIndx;
	private boolean justInRoom;
	private Integer roomCoordX;
	private Integer roomCoordY;

	// Defines the player class
	@SuppressWarnings("unchecked")
	public Player(String nameArg, int x, int y, int indivHand) {
		_cursorX = x;
		_cursorY = y;
		_roomX = null;
		_roomY = null;
		roomCoordX = null;
		roomCoordY = null;
		_playerName = nameArg;
		Board._boardDimensions[x][y] = nameArg;
		_myTurn = false;
		_currentTile = "Hallway";
		_movedFromTile = "";
		_playerHand = _cards.getListOfHands().get(indivHand);
		_inARoom = false;
		inRoom = "";
		playerIndx = 0;
		justInRoom = false;

	}

	// Creates the players (Names can be changed)
	public static void createPlayers(int numOfPlayers) {

		_cards = new Cards(numOfPlayers);

		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		listOfPlayers.add(player6);

		_cards.takeListOfPlayers(listOfPlayers);
	}

	// Returns a random number between 1 & 6
	public int rollDice() {
		_movesMade = 0;
		Random rand = new Random();
		_randomNumber = rand.nextInt(10) + 2;
		return _randomNumber; 
	}

	// Moves the player 1 place left
	public void moveLeft(Player player) throws InvalidMoveException {

		int x = player.getX();
		int y = player.getY();
		boolean myTurnAndCanMove = _movesMade < _randomNumber && player.isMyTurn() == true && player._cursorX > 0;
		boolean leftTileIsValid = Board._boardDimensions[x - 1][y] == "Hallway"
				|| Board._boardDimensions[x - 1][y] == "DoorEntrance";

		if (myTurnAndCanMove) {
			if (leftTileIsValid) {
				player._movedFromTile = player._currentTile;
				player._currentTile = Board._boardDimensions[x - 1][y];
				Board._boardDimensions[x - 1][y] = player.getName(player);
				Board._boardDimensions[x][y] = player._movedFromTile;
				player._cursorX = (x - 1);

				_movesMade++;
			}

			else if (Board._boardDimensions[x - 1][y] == "Door" && player._currentTile == "DoorEntrance") {
				// TODO Make the player enter the room and execute other
				// methods
				String roomName = Board._boardDimensions[x - 2][y];
				moveIntoRoom(player, roomName);
//				new InterClassInteractions("enter", player, roomName);
			}

			else {
				throw new InvalidMoveException();
			}
		}
		// player._cursorX = (x - 1);
		if (_movesMade == _randomNumber) {
			// _movesMade = 0;
			player.setTurnFalse();
		}

	}

	// Moves the player 1 place Right
	public void moveRight(Player player) throws InvalidMoveException {
		int x = player.getX();
		int y = player.getY();
		boolean myTurnAndCanMove = _movesMade < _randomNumber && player.isMyTurn() == true && player._cursorX < 23;
		boolean rightTileIsValid = Board._boardDimensions[x + 1][y] == "Hallway"
				|| Board._boardDimensions[x + 1][y] == "DoorEntrance";

		if (myTurnAndCanMove) {

			if (rightTileIsValid) {
				player._movedFromTile = player._currentTile;
				player._currentTile = Board._boardDimensions[x + 1][y];
				Board._boardDimensions[x + 1][y] = player.getName(player);
				Board._boardDimensions[x][y] = player._movedFromTile;
				player._cursorX = (x + 1);
				_movesMade++;
			}

			else if (Board._boardDimensions[x + 1][y] == "Door" && player._currentTile == "DoorEntrance") {
				// TODO Make the player enter the room and execute other
				// methods
				String roomName = Board._boardDimensions[x+2][y];
				moveIntoRoom(player, roomName);
//				new InterClassInteractions("enter", player, roomName);
			}

			else {
				throw new InvalidMoveException();
			}
		}
		// player._cursorX = (x + 1);
		if (_movesMade == _randomNumber) {
			// _movesMade = 0;
			player.setTurnFalse();
		}

	}

	// Moves the player 1 place Up
	public void moveUp(Player player) throws InvalidMoveException {

		int x = player.getX();
		int y = player.getY();

		if (_movesMade < _randomNumber && player.isMyTurn() == true && player._cursorY > 0) {
			if (Board._boardDimensions[x][y - 1] == "Hallway" || Board._boardDimensions[x][y - 1] == "DoorEntrance") {
				player._movedFromTile = player._currentTile;
				player._currentTile = Board._boardDimensions[x][y - 1];
				Board._boardDimensions[x][y - 1] = player.getName(player);
				Board._boardDimensions[x][y] = player._movedFromTile;
				player._cursorY = (y - 1);
				_movesMade++;
			}

			else if (Board._boardDimensions[x][y - 1] == "Door" && player._currentTile == "DoorEntrance") {

				// TODO Make the player enter the room and execute other
				// methods
				String roomName = Board._boardDimensions[x][y - 2];
				moveIntoRoom(player, roomName);
			}

			else {
				throw new InvalidMoveException();
			}
		}
		// player._cursorY = (y - 1);
		if (_movesMade == _randomNumber) {
			// _movesMade = 0;
			player.setTurnFalse();
		}

	}

	// Moves the player 1 place Down.
	public void moveDown(Player player) throws InvalidMoveException {

		int x = player.getX();
		int y = player.getY();

		if (_movesMade < _randomNumber && player.isMyTurn() == true && player._cursorY < 24) {
			if (Board._boardDimensions[x][y + 1] == "Hallway" || Board._boardDimensions[x][y + 1] == "DoorEntrance") {
				player._movedFromTile = player._currentTile;
				player._currentTile = Board._boardDimensions[x][y + 1];
				Board._boardDimensions[x][y + 1] = player.getName(player);
				Board._boardDimensions[x][y] = player._movedFromTile;
				player._cursorY = (y + 1);
				_movesMade++;
			}

			else if (Board._boardDimensions[x][y + 1] == "Door" && player._currentTile == "DoorEntrance") {

				// TODO Make the player enter the room and execute other
				// methods
				String roomName = Board._boardDimensions[x][y + 2];
				moveIntoRoom(player, roomName);
			}

			else {
				throw new InvalidMoveException();
			}
		}
		// player._cursorY = (y + 1);
		if (_movesMade == _randomNumber) {
			// _movesMade = 0;
			player.setTurnFalse();
		}

	}

	public void moveIntoRoom(Player p, String roomName) {
		Room room = null;
		for (Room r : Board.listOfRooms) {
			if (r.getRoomName().compareTo(roomName) == 0) {
				room = r;
			}
		}
		p._movedFromTile = p._currentTile;
		_roomX = p._cursorX;
		_roomY = p._cursorY;
		Board._boardDimensions[p._cursorX][p._cursorY] = p._movedFromTile;
		p._cursorX = room.getXCentOfRoom();
		p._cursorY = room.getYCentOfRoom();
		room.addToRoom(p);
		p.setInARoom(true);
		_movesMade = _randomNumber = 0;

	}
	
	public void exitRoom(Player p){
		p._cursorX = p._roomX;
		p._cursorY = p._roomY;
		p._roomX = null; p._roomY = null;
		p.setInARoom(false);
		p.justInRoom = true;
	}

	public void secretSpace(Player p, String roomName) {

		if (p.getInARoom() && p.getInRoom().equals(roomName)) {

			if (roomName.equals("Kitchen")) {
				moveIntoRoom(p, "Study");
			}
			if (roomName.equals("Study")) {
				moveIntoRoom(p, "Kitchen");
			}
			if (roomName.equals("Lounge")) {
				moveIntoRoom(p, "Conservatory");
			}
			if (roomName.equals("Conservatory")) {
				moveIntoRoom(p, "Lounge");
			}
		} // else
		// throw new NopetepousException();

	}


	// Returns the player's name
	public String getName(Player p) {
		return p._playerName;
	}

	// Returns the number of moves made
	public int getMovesMade() {
		return _movesMade;
	}

	// Returns players hand
	public List<String> getHand() {
		return _playerHand;
	}

	public static ArrayList<Player> getListOfPlayers() {
		return listOfPlayers;
	}

	// Returns the player's x position
	public int getX() {
		return _cursorX;
	}

	// Returns the player's y position
	public int getY() {
		return _cursorY;
	}

	// Starts a player's turn
	public void startTurn() {
		_myTurn = true;
		if (Room.getPlayersInRoom().contains(this)){
			exitRoom(this);
		}

	}

	// Sets a players turn to false
	public void setTurnFalse() {
		_myTurn = false;
		_movesMade = 0;
		_randomNumber = 0;
		setNextPlayerTurn();
	}

	private void setNextPlayerTurn() {
		if (playerIndx < 5) {
			playerIndx++;
			Player p = listOfPlayers.get(playerIndx);
			p.startTurn();
			System.out.println("setNextPlayerTurn has been called /n");
			System.out.println("startTurn() has been called on: " + p.getName(p));
		} else if (playerIndx == 5) {
			playerIndx = -1;
			setNextPlayerTurn();
		}
	}

	// Returns whether it's a player's turn or not
	public boolean isMyTurn() {
		return _myTurn;
	}

	public boolean setInARoom(boolean b) {
		return _inARoom = b;
	}

	public boolean getInARoom() {
		return _inARoom;
	}

	public void setInRoom(String s) {
		inRoom = s;
	}

	public String getInRoom() {
		return inRoom;
	}

	public int getRoll() {
		return _randomNumber;
	}

	public String getCurrentTile() {
		return _currentTile;
	}

	public String getMovedFromTile() {
		return _movedFromTile;

	}

	public static int getMovesLeft() {
		return _randomNumber - _movesMade;
	}
	
	public int getRoomX(){
		return _roomX;
	}
	
	public int getRoomY(){
		return _roomY;
	}
	
	public boolean inRoomFlag(){
		return justInRoom;
	}
	
	public Integer getRoomCoordX(){
		return roomCoordX;
	}
	
	public Integer getRoomCoordY(){
		return roomCoordY;
	}
	
	public void setRoomCoordX(int x){
		roomCoordX = x;
	}
	
	public void setRoomCoordY(int y){
		roomCoordY = y;
	}
	
	public static Player getCurrentPlayer(){
		for (Player p : Player.getListOfPlayers()){
			if (p.isMyTurn()){
				return p;
			}
		}
		return null;
	}
	
	public static void createPlayers1(int numOfPlayers) {
		_cards = new Cards(numOfPlayers);
	}
	
	public static void createPlayers2(int numOfPlayers) {
		_cards.takeListOfPlayers(listOfPlayers);
	}
	
}
