package edu.buffalo.cse116;

import javax.swing.JTextField;

public class TakeInput {
	
	private static String _person;
	private static String _weapon;
	private static String _room;
	
	public TakeInput(JTextField s1, JTextField s2, JTextField s3){
		_person = s1.getText();
		_weapon = s2.getText();
		_room = s3.getText();
	}
	
	public static String getPerson(){
		return _person;
	}
	
	public static String getWeapon(){
		return _weapon;
	
	}
	
	public static String getRoom(){
		return _room;
	}

}
