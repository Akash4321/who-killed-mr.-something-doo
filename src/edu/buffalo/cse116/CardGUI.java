package edu.buffalo.cse116;


import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CardGUI {

	private static ArrayList<Player> listOfPlayers = new ArrayList<Player>();
	private GUI gui;
	private JPanel rightPanel;
	private HashMap<String, JLabel> listOfCards;

	public CardGUI(GUI g) {
		gui = g;
		listOfCards = new HashMap<String, JLabel>();
		JFrame window = gui.getWindow();
		rightPanel = new JPanel();
		window.add(rightPanel);
		rightPanel.setVisible(true);
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		rightPanel.add(new JLabel("I am going to kill you"));
		try {
			cardAssociation();
		} catch (IOException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void cardAssociation() throws IOException, URISyntaxException {
		ArrayList<String> allCards = Cards.getAllCards();

		for (String card : allCards) {
			if (card != "ProfessorPlum") {
//				System.out.println(card.replaceAll("\\s+", ""));
				URI file = getClass().getResource("Images/" + card.replaceAll("\\s+", "") + ".jpg").toURI();
				BufferedImage image = ImageIO.read(new File(file));
				JLabel label = new JLabel(new ImageIcon(image));
				listOfCards.put(card, label);
			} else {
				URI file = getClass().getResource("Images/" + card.replaceAll("\\s+", "") + ".gif").toURI();
				BufferedImage image = ImageIO.read(new File(file));
				JLabel label = new JLabel(new ImageIcon(image));
				listOfCards.put(card, label);
			}
		}
	}

	public void displayCards(Player p) {

		rightPanel.removeAll();
		List<String> playerHand = p.getHand();
		for(String card: playerHand){
			JLabel img = listOfCards.get(card);
			img.setSize(new Dimension(rightPanel.WIDTH, rightPanel.HEIGHT/playerHand.size()));
			rightPanel.add(img);
		}

	}

}
