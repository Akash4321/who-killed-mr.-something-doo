package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

public class RollAction implements ActionListener {
	
	private JButton JL;
	int _rollNum;
	private GUI gui;
	private JButton rollButton;
	
	public RollAction(JButton j, GUI g, JButton r){
		JL = j;
		gui = g;
		rollButton = r;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ArrayList<Player> players = Player.getListOfPlayers();
		for (Player p : players) {
			if(p.isMyTurn()==true){
				p.rollDice();
				int num = p.getRoll();
				_rollNum = num;
		    	setText(num);
		    	setEnableButtons(p);
//		    	System.out.println("RollNumber: " +_rollNum);
//		    	System.out.println("Moves Made: " + p.getMovesMade());
//		    	System.out.println("X: " + p.getX() + " Y: " + p.getY());
		    	gui.cGUI.displayCards(p);
			}
		}
		
	}
	
	public void setText(int num){
		JL.setText("You have " + num + " moves left.");
		
	}
	
	public void setEnableButtons(Player currentPlayer){
		if (currentPlayer.getMovesLeft()== 0){
			gui.getButtons().setDisableButtons(false);
			rollButton.setEnabled(true);
		}else {
			gui.getButtons().setDisableButtons(true);
			rollButton.setEnabled(false);
		}
		
	}
	
	public int getRollNum(){
		return _rollNum;
	}
}
