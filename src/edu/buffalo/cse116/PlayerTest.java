package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.rules.ExpectedException;

import edu.buffalo.cse116.Player;
public class PlayerTest extends Board{
	
	

	@Test
	public void testMoveUp() throws Exception {
		new Board();
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		player1.startTurn();
		player1.rollDice();
		player1.moveUp(player1);
		String result = _boardDimensions[7][23];
		assertEquals("Miss Scarlet", result);
	}
	
	@Test
	public void testMoveDown() throws Exception {
		new Board();
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		player3.startTurn();
		player3.rollDice();
    	player3.moveDown(player3);
		String result = Board._boardDimensions[9][1];
		assertEquals("Mr. Green", result);
	}
	
	@Test
	public void testMoveLeft() throws Exception {
		new Board();
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		player5.startTurn();
		player5.rollDice();
		player5.moveLeft(player5);
		String result = Board._boardDimensions[22][6];
		assertEquals("Mrs. Peacock", result);
	}
	
	@Test
	public void testMoveRight() throws Exception {
		new Board();
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		player2.startTurn();
		player2.rollDice();
		player2.moveRight(player2);
		String result = Board._boardDimensions[1][17];
		assertEquals("Professor Plum", result);
	}
	
	@Test
	public void testGetX(){
		Player.createPlayers(6);	
		int result = Player.listOfPlayers.get(1).getX();
		assertEquals(0, result);
	}
	
	public void testGetY(){
		Player.createPlayers(6);	
		int result = Player.listOfPlayers.get(1).getY();
		assertEquals(17, result);
	}
	
	@Test
	public void testMoves() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player1.startTurn();
		player1._randomNumber = 3;
		player1.moveUp(player1);
		player1.moveUp(player1);
		int result = player1.getMovesMade();
    	assertEquals(2, result);
	}
	
	@Test
	public void test3MovesVerticalLegal() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 8, 10, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player5.startTurn();
		player5._randomNumber = 5;
		player5.moveDown(player5);
		player5.moveDown(player5);
		player5.moveDown(player5);
		String result = Board._boardDimensions[8][13];
		assertEquals("Mrs. Peacock", result);
	}
	
	@Test
	public void test3MovesHorizontalLegal() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player5.startTurn();
		player5._randomNumber = 5;
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		String result = Board._boardDimensions[20][6];
		assertEquals("Mrs. Peacock", result);
	}
	
	@Test
	public void test5MovesHorizontalAndVerticalLegal() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player5.startTurn();
		player5._randomNumber = 5;
		player5.moveLeft(player5);
		player5.moveDown(player5);
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		player5.moveUp(player5);
		String result = Board._boardDimensions[20][6];
		assertEquals("Mrs. Peacock", result);
	}
	
	@Test
	public void testifDoorLegal() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 6, 16, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player5.startTurn();
		player5._randomNumber = 9;
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		player5.moveLeft(player5);
		player5.moveUp(player5);
		player5.moveRight(player5);
		Boolean result = player5._inARoom;
		assertEquals(true, result);
	}
	
	@Test
	public void testMoreSquaresIllegal() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player2.startTurn();
		player2._randomNumber = 2;
		player2.moveRight(player2);
		player2.moveRight(player2);
		player2.moveRight(player2);
		String result = Board._boardDimensions[3][17];
		assertEquals("Hallway", result);
		
		// The player tried to move too many times and therefore the test should return Hallway
		// because the player wasn't able to move down a third time. 
	}
	
	
	@Test(expected = Exception.class)
	public void testMoveThroughWallIllegal() throws Exception {
		new Board();
		Player.createPlayers1(6);
		Player.listOfPlayers = new ArrayList<Player>();
		Player player1 = new Player("Miss Scarlet", 7, 24, 0);
		Player.listOfPlayers.add(player1);
		Player player2 = new Player("Professor Plum", 0, 17, 1);
		Player.listOfPlayers.add(player2);
		Player player3 = new Player("Mr. Green", 9, 0, 5);
		Player.listOfPlayers.add(player3);
		Player player4 = new Player("Mrs. White", 14, 0, 2);
		Player.listOfPlayers.add(player4);
		Player player5 = new Player("Mrs. Peacock", 23, 6, 3);
		Player.listOfPlayers.add(player5);
		Player player6 = new Player("Colonel Mustard", 23, 19, 4);
		Player.listOfPlayers.add(player6);
		Player.createPlayers2(6);
		player2.startTurn();
		player2._randomNumber = 2;
		player2.moveLeft(player2);
		
		// This Test passes because of the (expected = Exception.class code). The test returns an InvalidMoveException because this is an invalid move.
		}
	
	@Test
	public void testDiagonalIllegal() throws Exception {		
		// It is impossible for the player to move diagonally with our code. This is because in order to move, either
		// the moveLeft(), moveRight(), moveUp(), or moveDown() methods have to be called. Therefore, the players can only move adjacently
		}
	
	@Test
	public void testContiguousIllegal() throws Exception {		
		// It is impossible for the player to an uncontiguous square with our code. This is because in order to move, either
		// the moveLeft(), moveRight(), moveUp(), or moveDown() methods have to be called.
		// Therefore, the player will only be able to move one square at a time until they run out of moves.
		}
	
	@Test
	public void testSecretPassage() throws Exception {		
		// TODO
		}
	
	
	


}
