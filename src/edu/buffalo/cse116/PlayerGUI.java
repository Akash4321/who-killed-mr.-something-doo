package edu.buffalo.cse116;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PlayerGUI {

	private ArrayList<Player> listOfPlayers = Player.getListOfPlayers();

	private int x;
	private int y;
	private JLabel[][] gd;
	private GUI gui;
	private JLabel roomTile;
	private JDialog _suggWind;
	private JTextField _jTF1;
	private JTextField _jTF2;
	private JTextField _jTF3;
	protected static JDialog suggWindow;

	public PlayerGUI(GUI g, JLabel[][] GUIDimensions) {
		gd = GUIDimensions;
		gui = g;
		for (Player p : listOfPlayers) {
			paintPlayer(p);
		}
		listOfPlayers.get(0).startTurn();
	}

	public void paintPlayer(Player p) {
		if (!p.getInARoom()) {
			x = p.getX();
			y = p.getY();
			JLabel playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
		}else{
			paintPlayerIntoRoom(p);
			roomTile = null;
		}
	}

	public Color colorAssociation(Player p) {

		if (p.getName(p).compareTo("Miss Scarlet") == 0) {

			return new Color(220, 20, 60);
		}
		if (p.getName(p).compareTo("Colonel Mustard") == 0) {
			return Color.YELLOW;
		}
		if (p.getName(p).compareTo("Professor Plum") == 0) {
			return new Color(186, 85, 211);
		}
		if (p.getName(p).compareTo("Mr. Green") == 0) {
			return new Color(0, 255, 0);
		}
		if (p.getName(p).compareTo("Mrs. White") == 0) {
			return Color.WHITE;
		}
		if (p.getName(p).compareTo("Mrs. Peacock") == 0) {
			return Color.CYAN;
		}

		return null;
	}

	public void movePlayerLeft(Player p) throws InvalidMoveException {

		p.moveLeft(p);
		paintPlayer(p);
		repaint(p, "LEFT");
		gui.buttons.paintPlayerLabel(Player.getCurrentPlayer());

	}

	public void movePlayerRight(Player p) throws InvalidMoveException {

		p.moveRight(p);
		paintPlayer(p);
		repaint(p, "RIGHT");
		gui.buttons.paintPlayerLabel(Player.getCurrentPlayer());

	}

	public void movePlayerUp(Player p) throws InvalidMoveException {
		p.moveUp(p);
		paintPlayer(p);
		repaint(p, "UP");
		gui.buttons.paintPlayerLabel(Player.getCurrentPlayer());

	}

	public void movePlayerDown(Player p) throws InvalidMoveException {

		p.moveDown(p);
		paintPlayer(p);
		repaint(p, "DOWN");
		gui.buttons.paintPlayerLabel(Player.getCurrentPlayer());

	}

	public void repaint(Player p, String Direction) {
		JLabel previousTile = new JLabel();
		if (!p.getInARoom()) {
			if (Direction == "UP") {
				previousTile = gd[p.getX()][p.getY() + 1];
			}
			if (Direction == "DOWN") {
				previousTile = gd[p.getX()][p.getY() - 1];
			}
			if (Direction == "LEFT") {
				previousTile = gd[p.getX() + 1][p.getY()];
			}
			if (Direction == "RIGHT") {
				System.out.println("p.X: " + p.getX() + " p.Y: " + p.getY());
				previousTile = gd[p.getX() - 1][p.getY()];
			}
			previousTile.setBackground(new Color(250, 240, 230));
		}
	}
	
	public void paintPlayerIntoRoom(Player p){
		int x = p.getX();
		int y = p.getY();
		int dEntX = p.getRoomX();
		int dEntY = p.getRoomY();
		
		gd[dEntX][dEntY].setBackground(GUI.DoorEntrance);// re-color Door Entrance
		JLabel playerTile;
		
		if (gd[x][y].getBackground() == GUI.Room){
		
			//paints that tile into player color
			playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
			p.setRoomCoordX(x);
			p.setRoomCoordY(y);
		}
		
		else if(gd[x+1][y].getBackground() == GUI.Room){
			x = x +1;
			playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
			p.setRoomCoordX(x);
			p.setRoomCoordY(y);
			
		}
		
		else if (gd[x][y+1].getBackground() == GUI.Room){
			y = y +1;
			playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
			p.setRoomCoordX(x);
			p.setRoomCoordY(y);
		}
		else if (gd[x-1][y].getBackground() == GUI.Room){
			x = x-1;
			playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
			p.setRoomCoordX(x);
			p.setRoomCoordY(y);
		}
		else if (gd[x][y-1].getBackground() == GUI.Room){
			y = y -1;
			playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
			p.setRoomCoordX(x);
			p.setRoomCoordY(y);
		}
		else if (gd[x+1][y-1].getBackground() == GUI.Room){
			x = x+1;
			y = y-1;
			playerTile = gd[x][y];
			playerTile.setBackground(colorAssociation(p));
			p.setRoomCoordX(x);
			p.setRoomCoordY(y);
		}
		makeSuggBox(p, x, y);
	}
	
	public void paintPlayerOutOfRoom(Player p){
		System.out.println("The code reached this line");
		int roomx = p.getRoomCoordX();
		int roomy = p.getRoomCoordY();
		JLabel roomTile = gd[roomx][roomy];
		roomTile.setBackground(GUI.Room);
		paintPlayer(p);
	}

	public void makeSuggBox(Player p, int x, int y){
		
		suggWindow = new JDialog();
		JPanel suggPan = new JPanel();
		_jTF1 = new JTextField(30);
		_jTF2 = new JTextField(30);
		_jTF3 = new JTextField(30);
		JButton suggButton = new JButton("Suggest");
		
		suggPan.add(_jTF1);
		suggPan.add(_jTF2);
		suggPan.add(_jTF3);
		suggPan.add(suggButton);
		suggWindow.add(suggPan);
		suggWindow.setVisible(true);
		suggWindow.setSize(400, 200);
		suggWindow.setDefaultCloseOperation(suggWindow.HIDE_ON_CLOSE);
		
		
		TakeInput input = new TakeInput(_jTF1, _jTF2, _jTF3);
		
		suggButton.addActionListener(new SuggestionListener(input, p, getRoomName(x,y)));

		_suggWind = suggWindow;
	}
	
	private String getRoomName(int x, int y){
		return Board._boardDimensions[x][y];
	}
	
	public JDialog getSuggWindow(){
		return _suggWind;
	}
	
	public JTextField getJTF1(){
		return _jTF1;
	}
	
	public JTextField getJTF2(){
		return _jTF2;
	}
	public JTextField getJTF3(){
		return _jTF3;
	}
	
}
