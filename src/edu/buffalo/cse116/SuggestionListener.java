package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class SuggestionListener implements ActionListener{
	private PlayerGUI gui;
	private String _person;
	private String _weapon;
	private String _room;
	private TakeInput Input;
	private Player p;
		
	
	public SuggestionListener(TakeInput tI, Player P, String roomName){
		Input = tI;
		p = p;
		_room= roomName;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
		new InterClassInteractions(p, _room).makeSuggestion(Input);
		}
		catch (NopetepousException E){
			System.err.println("Either you spelled something wrong, entered something that is not in the game,");
			System.err.println("or its in an order other than person, weapon, room");
		}
	}
	
}
