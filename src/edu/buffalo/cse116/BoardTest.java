package edu.buffalo.cse116;

import static org.junit.Assert.*;

import org.junit.Test;

public class BoardTest {

	
	
	
	@Test
	public void testKitchen() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[4][5];
		
		assertEquals("Kitchen", result);
	}
	
	@Test
	public void testDining() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[6][11];
		
		assertEquals("DiningRoom", result);
	}
	
	@Test
	public void testLounge() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[4][22];
		
		assertEquals("Lounge", result);
	}
	
	@Test
	public void testHall() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[10][24];
		
		assertEquals("Hall", result);
	}
	
	@Test
	public void testStudy() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[23][22];
		
		assertEquals("Study", result);
	}
	
	@Test
	public void testLibrary() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[18][18];
		
		assertEquals("Library", result);
	}
	
	@Test
	public void testBilliard() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[18][11];
		
		assertEquals("BillardRoom", result);
	}
	
	@Test
	public void testConservatory() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[23][2];
		
		assertEquals("Conservatory", result);
	}
	
	@Test
	public void testBallroom() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[9][4];
		
		assertEquals("BallRoom", result);
	}

	@Test
	public void testCenter() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[12][13];
		
		assertEquals("CENTER", result);
	}
	
	@Test
	public void testDoor() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[12][18];
		
		assertEquals("Door", result);
	}
	
	@Test
	public void testHallway() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[7][17];
		
		assertEquals("Hallway", result);
	}
	
	@Test
	public void testNullSpaces() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[7][0];
		
		assertEquals(null, result);
	}
	
	@Test
	public void testSecret() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[22][5];
		System.out.println(test._boardDimensions[22][5]);
		
		assertEquals("ConservatorySP", result);
	}
	
	@Test
	public void testEntrance() {

		Board test = new Board();
		
		String[][] dimensions = test.getBoardDimensions();
		String result = dimensions[9][8];
		
		assertEquals("DoorEntrance", result);
	}
}
