package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ButtonListener implements ActionListener {

	private GUI gui;
	private Player currentPlayer;
	private String d;
	List<Player> pList;

	public ButtonListener(GUI g, String Direction) {
		gui = g;
		d = Direction;
		pList = Player.getListOfPlayers();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		currentPlayer = getCurrentPlayer();
		
		try {
			
			if (d == "UP") {
				gui.pGUI.movePlayerUp(currentPlayer);
			}
			if (d == "DOWN") {
				gui.pGUI.movePlayerDown(currentPlayer);
			}
			if (d == "LEFT") {
				gui.pGUI.movePlayerLeft(currentPlayer);
			}
			if (d == "RIGHT") {
				gui.pGUI.movePlayerRight(currentPlayer);
			}
			gui.getRollA().setEnableButtons(currentPlayer);
			if (!currentPlayer._myTurn){
				Player p = getCurrentPlayer();
				if (p.inRoomFlag()){
					gui.pGUI.paintPlayerOutOfRoom(p);
				}
			}
		} catch (InvalidMoveException E) {
			System.err.println("That is an invalid move. Please try a different one");
		}

		gui.getRollA().setText(currentPlayer.getMovesLeft());
		System.out.println("RollNumber: " + currentPlayer.getRoll());
		System.out.println(currentPlayer.getName(currentPlayer) + "Moves Made: " + currentPlayer.getMovesMade());
		System.out.println("Direction Moved: " + d);
		System.out.println("X: " + currentPlayer.getX() + " Y: " + currentPlayer.getY());
	}
	
	private Player getCurrentPlayer(){
		for (Player p : pList) {
			if (p._myTurn) {
				return p;
			}
		}
		return null;
	}

}
