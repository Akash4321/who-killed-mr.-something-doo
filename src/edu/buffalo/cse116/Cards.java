package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Cards {

	private ArrayList<String> _weaponCards;
	private ArrayList<String> _roomCards;
	private ArrayList<String> _playerCards;
	protected static ArrayList<Player> listOfPlayers;
	protected static ArrayList<String> allCards;
	@SuppressWarnings("rawtypes")
	private static ArrayList<List> listOfHands;
	protected static String _gotMurked;
	protected static String _withWhat;
	protected static String _where;
	protected static String _suggPerson;
	protected static String _suggRoom;
	protected static String _suggWeapon;

	public Cards() {
		createWeaponCards();
		createRoomCards();
		createPlayerCards();
		
	}

	public Cards(int numOfPlayers) {
		listOfHands = new ArrayList<List>();
		createWeaponCards();
		createRoomCards();
		createPlayerCards();
		dealCards(numOfPlayers);

	}

	public void createWeaponCards() {
		_weaponCards = new ArrayList<String>();
		_weaponCards.add("Wrench");
		_weaponCards.add("Candlestick");
		_weaponCards.add("LeadPipe");
		_weaponCards.add("Rope");
		_weaponCards.add("Revolver");
		_weaponCards.add("Knife");
	}

	public void createRoomCards() {
		
		_roomCards = new ArrayList<String>();
		_roomCards.add("Kitchen");
		_roomCards.add("DiningRoom");
		_roomCards.add("Lounge");
		_roomCards.add("Hall");
		_roomCards.add("Study");
		_roomCards.add("Library");
		_roomCards.add("BillardRoom");
		_roomCards.add("Conservatory");
		_roomCards.add("Ballroom");
	}

	public void createPlayerCards() {
		
		_playerCards = new ArrayList<String>();
		_playerCards.add("ColonelMustard");
		_playerCards.add("MissScarlet");
		_playerCards.add("ProfessorPlum");
		_playerCards.add("Mr.Green");
		_playerCards.add("Mrs.White");
		_playerCards.add("Mrs.Peacock");
	}

	public void shuffle() {
		Collections.shuffle(_playerCards);
		Collections.shuffle(_roomCards);
		Collections.shuffle(_weaponCards);
	}


	public void takeListOfPlayers(ArrayList<Player> L) {
		listOfPlayers = L;
	}

	// will add method that adds these cards to the center box later

	public ArrayList<String> murderEnvelope() {
		shuffle();
		ArrayList<String> retVal = new ArrayList<String>();
		_gotMurked = _playerCards.remove(0);
		retVal.add(_gotMurked);
		_where = _roomCards.remove(0);
		retVal.add(_where);
		_withWhat = _weaponCards.remove(0);
		retVal.add(_withWhat);
		System.out.println("Solution: " +_gotMurked +" "+_where+" "+_withWhat);
		return retVal;
	}

	@SuppressWarnings("unchecked")
	public void dealCards(int numPlayers) {
		ArrayList<String> murdCards = murderEnvelope();
		allCards = new ArrayList<String>();
		for (int i = 0; i < numPlayers; i++) {
			List<String> indivHand = new ArrayList<String>();
			listOfHands.add(indivHand);
		}
		ArrayList<String> reDeck = new ArrayList<String>();
		while (_playerCards.isEmpty() == false) {
			reDeck.add(_playerCards.remove(0));
		}
		while (_weaponCards.isEmpty() == false) {
			reDeck.add(_weaponCards.remove(0));
		}
		while (_roomCards.isEmpty() == false) {
			reDeck.add(_roomCards.remove(0));
		}
		
		allCards.addAll(reDeck);
		allCards.addAll(murdCards);

		Collections.shuffle(reDeck);
		while (reDeck.isEmpty() == false) {
			for (List<String> indivHand : listOfHands) {
				indivHand.add(reDeck.remove(0));
			}
		}

	}

	public static void accusationValidation(String person, String place, String weapon) {
		if (_gotMurked == person && _withWhat == weapon && _where == place) {
			System.out.println("You win");
		} else {
			System.out.println("Incorrect, you lose");
		}
	}

	public static boolean suggestionCheckCurrentPlayer(String person, String room, String weapon, Player player) {
		// int currentPlayerHand = listOfHands.indexOf(Player.getHand());//How
		// does compiler know which player is getting called on?
		// List <String> cpCards = listOfHands.get(currentPlayerHand);
		List<String> cpCards = player.getHand();
		if (cpCards.contains(weapon) || cpCards.contains(room) || cpCards.contains(person)) {
			return true;
		}
		return false;
	}

	public static boolean suggestionRot(String person, String room, String weapon, Player player) {

		ArrayList<Player> tempList = listOfPlayers;
		tempList.remove(player);
		boolean foundSomething = false;
		for(Player p : tempList){
			List<String> L = p.getHand();
			if (L.contains(weapon) || L.contains(room) || L.contains(person)) {
				revealWeapon(weapon);
				revealRoom(room);
				revealPerson(person);
				foundSomething = true;
			}
			if(foundSomething == true){
				break;
			}
		}
		if (foundSomething ==false && (suggestionCheckCurrentPlayer(person, room, weapon, player) == false) ){
			
			InterClassInteractions.makeAccusation(person, room, weapon);
		}
		return foundSomething;

	}

	public static String revealWeapon(String weapon) {
		return weapon;
	}

	public static  String revealRoom(String room) {
		return room;
	}

	public static String revealPerson(String person) {
		return person;
	}

	public ArrayList<List> getListOfHands() {
		return listOfHands;
	}

	public ArrayList<String> getPeople() {
		return _playerCards;
	}

	public ArrayList<String> getWeapons() {
		return _weaponCards;
	}

	public ArrayList<String> getRooms() {
		return _roomCards;
	}
	
	public static ArrayList<String> getAllCards(){
		return allCards;
	}
	

}