package edu.buffalo.cse116;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;

import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI {
	static JFrame _window;
	private Color color = Color.BLACK;
	static boolean isHallway;
	protected JPanel bottomBoard = new JPanel();
	protected JPanel gameBoard;
	protected JPanel leftPanel;
	protected JLabel[][] GUIDimensions;
	protected PlayerGUI pGUI;
	protected CardGUI cGUI;
	private RollAction rollA;
	public Buttons buttons;
	public static Color Hallway = new Color(250, 240, 230);
	public static Color NullSpace = Color.DARK_GRAY;
	public static Color DoorEntrance =  new Color(250, 240, 230);
	public static Color SecretSpace = new Color(216, 191, 216);
	public static Color Room = Color.LIGHT_GRAY;

	public GUI() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = screenSize.height;
		int width = screenSize.width * 2 / 3;
		_window = new JFrame("Definitely Not Clue");
		_window.setPreferredSize(new Dimension(width, height));
		_window.getContentPane().setLayout(new BoxLayout(_window.getContentPane(), BoxLayout.X_AXIS));
		_window.setVisible(true);
		_window.pack();
		_window.setDefaultCloseOperation(_window.EXIT_ON_CLOSE);
		paintBoard();
		makeDice();
		
		bottomBoard.setVisible(true);
		leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.add(gameBoard);
		leftPanel.add(bottomBoard);
		_window.add(leftPanel);
		
		pGUI = new PlayerGUI(this, GUIDimensions);
		cGUI = new CardGUI(this);
		buttons = new Buttons(this);
	}

	public void paintBoard() {
		GUIDimensions = new JLabel[24][25];
		gameBoard = new JPanel();
		gameBoard.setLayout(new GridLayout(25, 24));

		// create the chess board squares
		for (int y = 0; y < Board._boardDimensions[0].length; y++) {
			for (int x = 0; x < Board._boardDimensions.length; x++) {
				isHallway = Board.isHallway(x, y);
				JLabel b = new JLabel();
				b.setPreferredSize(new Dimension(20, 20));
				b.setOpaque(true);
				if (isHallway) {
					color = Hallway;
					b.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
				} else if (Board._boardDimensions[x][y] == null) {
					color = NullSpace;
					b.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
				} else if (Board._boardDimensions[x][y] == "DoorEntrance") {
					color = DoorEntrance;
				} else if (Board._boardDimensions[x][y] == "KitchenSP" || Board._boardDimensions[x][y] == "LoungeSP"
						|| Board._boardDimensions[x][y] == "ConservatorySP"
						|| Board._boardDimensions[x][y] == "StudySP") {
					color = SecretSpace;
				} else if (Board.isRoom(x, y)) {
					color = Room;
				} else {
					boolean addedPlayerTile = false;
					for (Player player : Player.listOfPlayers) {
						String name = player._playerName;
						if (name == Board._boardDimensions[x][y]) {
							color = Color.RED;
							b.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
							addedPlayerTile = true;
						}
					}
					if (!addedPlayerTile) {
						color = new Color(244, 164, 96);
						b.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
//						System.out.println("X: " + x + "      Y: " + y);
//						System.out.println(Board._boardDimensions[x][y]);
					}
				}
				b.setBackground(color);
				gameBoard.add(b);
				GUIDimensions[x][y] = b;
//				System.out.println("Added label");
			}
		}
//		_window.add(gameBoard);
		_window.repaint();
	}

	public void makeDice() {
		JButton roll = new JButton("Roll");
		JButton holdDie = new JButton();
		rollA = new RollAction(holdDie, this, roll);
		roll.addActionListener(rollA);
		roll.setVisible(true);
		bottomBoard.add(roll);
		bottomBoard.add(holdDie);
		holdDie.setVisible(true);
	}



	public JLabel[][] getGUIDimensions() {
		return GUIDimensions;
	}
	
	public RollAction getRollA(){
		return rollA;
	}
	
	public Buttons getButtons(){
		return buttons;
	}
	
	public JFrame getWindow(){
		return _window;
	}

}
