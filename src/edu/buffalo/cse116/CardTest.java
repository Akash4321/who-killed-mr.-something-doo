package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class CardTest extends Cards {

	@Test // Suggestion would be answered by the next player because they have
			// the Player card;
	public void testPlayerCardSuggestion() {
		
		Player.createPlayers(6);

		ArrayList<Player> playerList = listOfPlayers;
		
		Player testPlayer = playerList.get(3); //That should player4, which should be Mr. Green
		
		//Cards test = new Cards();
		// create and assign the envelope cards
		// NOTE to the comment above: the murder envelope basically just assigns
		// Strings to the instance variables _gotMurked, _withWhat and _where.
		// You cannot access how the method works, but by extending the cards
		// class, you can directly get the Strings you need
		
		
		String suggestPerson = _gotMurked;
		String suggestWhere = _where;
		String suggestWithWhat = _withWhat;

//		 put cards in the envelope
//		String cards = test.murderEnvelope();
//		 testing suggestion
//		cards = test.suggestionRot(person, room, weapon);
//		 assert statements
//		assertEquals("Miss Scarlet", person);
//		assertEquals("Kitchen", room);
//		assertEquals("Revolver", weapon);
		
		//This checks if the current player hand as the cards suggested (I think)?
		assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		//This checks if the other players has the cards suggested 
		assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
	}

	@Test // Suggestion would be answered by the next player because they have
			// the Room card;
	public void testRoomCardSuggestion() {
		
		Player.createPlayers(6);

		ArrayList<Player> playerList = listOfPlayers;
		
		Player testPlayer = playerList.get(3);
		
		String suggestPerson = _gotMurked;
		String suggestWhere = _where;
		String suggestWithWhat = _withWhat;
		
		assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
	}

	@Test // Suggestion would be answered by the next player because they have
			// the Weapon card;
	public void testWeaponCardSuggestion() {
		
		Player.createPlayers(6);

		ArrayList<Player> playerList = listOfPlayers;
		
		Player testPlayer = playerList.get(3);
		
		String suggestPerson = _gotMurked;
		String suggestWhere = _where;
		String suggestWithWhat = _withWhat;
		
		assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));

	}

	@Test // Suggestion would be answered by the next player because they have 2
			// matching cards;
	public void testTwoMatchingCards() {
		
		Player.createPlayers(6);
		 
		 ArrayList<Player> playerList = listOfPlayers;
		 
		 Player testPlayer = playerList.get(3);
		 Player testPlayerNext = playerList.get(4);
		 
		 String suggestPerson = _gotMurked;
		 String suggestWhere = _where;
		 String suggestWithWhat = _withWhat;
		 
		 assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayerNext));

	}

	@Test // Suggestion would be answered by the player after the next player
			// because they have 1 or more matching cards;
	public void testPlayerAfterNextPlayer() {
		
		 Player.createPlayers(6);
		 
		 ArrayList<Player> playerList = listOfPlayers;
		 
		 Player testPlayer = playerList.get(3);
		 Player testPlayerAfter = playerList.get(4);
		 
		 String suggestPerson = _gotMurked;
		 String suggestWhere = _where;
		 String suggestWithWhat = _withWhat;
		 
		 assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayerAfter));

	}

	@Test // Suggestion would be answered by the player immediately before
			// player making suggestion because they have 1 or more matching
			// cards;
	public void testPlayerImmediatelyBefore() {
		
		Player.createPlayers(6);
		 
		 ArrayList<Player> playerList = listOfPlayers;
		 
		 Player testPlayer = playerList.get(3);
		 Player testPlayerBefore = playerList.get(2);
		 
		 String suggestPerson = _gotMurked;
		 String suggestWhere = _where;
		 String suggestWithWhat = _withWhat;
		 
		 assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayerBefore));

	}

	@Test // Suggestion cannot be answered by any player but the player making
			// the suggestion has 1 or more matching cards;
	public void testCannotAnswerButPlayerMakingSuggestionHasMatchingCard() {
		
		Player.createPlayers(6);
		 
		 ArrayList<Player> playerList = listOfPlayers;
		 
		 Player testPlayer = playerList.get(3);
		 		 
		 String suggestPerson = _gotMurked;
		 String suggestWhere = _where;
		 String suggestWithWhat = _withWhat;
		 
		 assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 
	}

	@Test // Suggestion cannot be answered by any player and the player making
			// the suggestion does not have any matching cards.
	public void testCannotAnswerNoMatchingCards() {
		
		Player.createPlayers(6);
		 
		 ArrayList<Player> playerList = listOfPlayers;
		 
		 Player testPlayer = playerList.get(3);
		 
		 String suggestPerson = _gotMurked;
		 String suggestWhere = _where;
		 String suggestWithWhat = _withWhat;
		 
		 assertEquals(false, suggestionCheckCurrentPlayer(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));
		 assertEquals(false, suggestionRot(suggestPerson, suggestWhere, suggestWithWhat, testPlayer));

	}
}


