package edu.buffalo.cse116;

import java.util.ArrayList;

public class Room {
	
	private static ArrayList<Player> playersInRoom = new ArrayList <Player>();
	private int xCentOfRoom;
	private int yCentOfRoom;
	private String roomName;
	
	public Room(){
		System.out.println("Error! You need to provide coordinates for the rooms");
		throw new UnsupportedOperationException();
	}

	// Constructor for a 4-cornered room
	public Room(int y1, int y2, int x1, int x2, String[][] board, String name) {
		xCentOfRoom = (x1 + x2)/2;
		yCentOfRoom = (y1 + y2)/2;
		roomName = name;
		int temp = y1;
		for (; x1 <= x2; x1++) {// iterates through from 1st x-coordinate of to last
			y1= temp;			// resets y var so the second loop can run each time		
			for (; y1 <= y2; y1++) {// for each x coord, iterates through every
									// y coord of room and places the room name
									// on the tile
				board[x1][y1] = name;
				//System.out.println("You Better Fucking work");
			}
		}
		
//		System.out.println(roomName + " " + x1 + " + " + x2 + " / " + 2);
		System.out.println("Room: " + roomName + " centX: " + xCentOfRoom + " centY: " + yCentOfRoom);
	}

	// Constructor for a more-than-4-cornered room
	public Room(int y1, int y2, int x1, int x2, int y3, int y4, int x3, int x4, String[][] board, String name) {
		// split the room up into two rectangles and iterate through each rectangle
		//rect 1
		int temp = y1;
		int temp2 = y3;
		int x = 0;
		int y = 0;
		xCentOfRoom = (x1 + x2 + x3 + x4)/4;
		yCentOfRoom = (y1 + y2 +y3 + y4)/4;
		roomName = name;
//		System.out.println("TestROOMrect1");
		for (; x1 <= x2; x1++) {
			y1 = temp;
			for (; y1 <= y2; y1++) {
				
				board[x1][y1] = name;
//				System.out.println(x);
				x++;
			}
		}
		//rect2
//		System.out.println("TestROOMrect2");
		for (; x3 <= x4; x3++) {
			y3 = temp2;
			for (; y3 <= y4; y3++) {
				
				board[x3][y3] = name;
//				System.out.println(y);
				y++;
				
			}
		}
//		System.out.println("TestROOMend");
		
//		System.out.println(roomName + " " + x1 + " + " + x2 + " / " + 2);
		System.out.println("Room: " + roomName + " centX: " + xCentOfRoom + " centY: " + yCentOfRoom);
	}
	
	public int getXCentOfRoom(){
		return xCentOfRoom;
	}

	public int getYCentOfRoom(){
		return yCentOfRoom;
	}

	public void addToRoom(Player p){
		playersInRoom.add(p);
	}
	
	public void removeFromRoom(Player p){
		playersInRoom.remove(p);
	}
	
	public static ArrayList<Player> getPlayersInRoom(){
		return playersInRoom;
	}
	
	public String getRoomName(){
		return roomName;
	}
}
