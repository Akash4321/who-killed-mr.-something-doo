package edu.buffalo.cse116;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

public class Buttons {
	
	private JButton playerLabel;
	private JButton UP;
	private JButton DOWN;
	private JButton LEFT;
	private JButton RIGHT;
	private GUI gui;
	
	public Buttons(GUI g){
		ArrayList<Player> listOfPlr = Player.getListOfPlayers();
		gui = g;
		
		playerLabel = new JButton();
		paintPlayerLabel(Player.getCurrentPlayer());
		UP = new JButton("UP");
		UP.addActionListener(new ButtonListener(gui,"UP"));
		DOWN = new JButton("DOWN");
		DOWN.addActionListener(new ButtonListener(gui,"DOWN"));
		LEFT = new JButton("LEFT");
		LEFT.addActionListener(new ButtonListener(gui,"LEFT"));
		RIGHT = new JButton("RIGHT");
		RIGHT.addActionListener(new ButtonListener(gui,"RIGHT"));
		
		g.bottomBoard.add(playerLabel);
		g.bottomBoard.add(UP);
		g.bottomBoard.add(DOWN);
		g.bottomBoard.add(LEFT);
		g.bottomBoard.add(RIGHT);
		
		if (Player.getMovesLeft() == 0){
			setDisableButtons(false);
			
		}
		
	}
	
	public GUI getGUI(){
		return gui;
	}
	
	public void setDisableButtons(boolean b){
		UP.setEnabled(b);
		DOWN.setEnabled(b);
		LEFT.setEnabled(b);
		RIGHT.setEnabled(b);
	}
	
	public void paintPlayerLabel(Player p){
		playerLabel.setText(p.getName(p) + "'s turn");
		playerLabel.setBackground(gui.pGUI.colorAssociation(p));
	}
	

}
