package edu.buffalo.cse116;

import java.awt.EventQueue;

public class Driver {
	
	public static void main (String args[]){
		new Board();
		Player.createPlayers(6);
		EventQueue.invokeLater(new Runnable()
		{
		    public void run()
		    {
		    	new GUI();
		    }
		});
	}

}
